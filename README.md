## Description
Precor Connect customer segment service SDK for javascript.

## Features

##### List Level 1 Customer Segments
* [documentation](features/ListLevel1CustomerSegments.feature)

##### List Level 2 Customer Segments
* [documentation](features/ListLevel2CustomerSegments.feature)

##### List Level 3 Customer Segments
* [documentation](features/ListLevel3CustomerSegments.feature)

## Setup

**install via jspm**  
```shell
jspm install customer-segment-service-sdk=bitbucket:precorconnect/customer-segment-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import CustomerSegmentServiceSdk,{CustomerSegmentServiceSdkConfig} from 'customer-segment-service-sdk'

const customerSegmentServiceSdkConfig = 
    new CustomerSegmentServiceSdkConfig(
        "https://customer-segment-service-dev.precorconnect.com"
    );
    
const customerSegmentServiceSdk = 
    new CustomerSegmentServiceSdk(
        customerSegmentServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```