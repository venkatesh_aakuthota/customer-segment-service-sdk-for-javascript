import CustomerSegmentServiceSdkConfig from './customerSegmentServiceSdkConfig';
import DiContainer from './diContainer';
import Level1CustomerSegmentView from './level1CustomerSegmentView';
import Level2CustomerSegmentView from './level2CustomerSegmentView';
import Level3CustomerSegmentView from './level3CustomerSegmentView';
import ListLevel1CustomerSegmentsFeature from './listLevel1CustomerSegmentsFeature';
import ListLevel2CustomerSegmentsFeature from './listLevel2CustomerSegmentsFeature';
import ListLevel3CustomerSegmentsFeature from './listLevel3CustomerSegmentsFeature';

/**
 * @class {CustomerSegmentServiceSdk}
 */
export default class CustomerSegmentServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {CustomerSegmentServiceSdkConfig} config
     */
    constructor(config:CustomerSegmentServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    listLevel1CustomerSegments(accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(ListLevel1CustomerSegmentsFeature)
            .execute(accessToken);

    }

    listLevel2CustomerSegments(accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(ListLevel2CustomerSegmentsFeature)
            .execute(accessToken);
    }

    listLevel3CustomerSegments(accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(ListLevel3CustomerSegmentsFeature)
            .execute(accessToken);

    }

}
