import CustomerSegmentView from './customerSegmentView';

/**
 * @class {Level1CustomerSegmentView}
 */
export default class Level1CustomerSegmentView extends CustomerSegmentView {

    /**
     * @param {number} id
     * @param {string} name
     */
    constructor(id:number,
                name:string) {

        super(id, name);

    }

}
